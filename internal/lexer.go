package internal

import (
	"io"
	"os"
	"unicode"

	"github.com/pkg/errors"
)

type Lexer struct {
	path   string
	source string
	cur    uint
	bol    uint
	row    uint
}

func NewLexer(path string) (*Lexer, error) {
	buf, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.Wrapf(err, "could not read input file %s\n", path)
	}

	if len(buf) <= 0 {
		return nil, errors.Wrapf(io.EOF, "could not read input file %s\n", path)
	}

	return &Lexer{
		path:   path,
		source: string(buf),
		cur:    0,
		bol:    0,
		row:    0,
	}, nil
}

func (l *Lexer) is_empty() bool {
	return !l.is_not_empty()
}

func (l *Lexer) is_not_empty() bool {
	return l.cur < uint(len(l.source))
}

func (l *Lexer) advance() {
	if l.is_not_empty() {
		c := l.source[l.cur]
		l.cur += 1
		if c == '\n' {
			l.bol = l.cur
			l.row += 1
		}
	}
}

func (l *Lexer) trim_left() {
	for {
		if l.is_empty() {
			return
		}

		c := rune(l.source[l.cur])
		if !unicode.IsSpace(c) {
			return
		}

		l.advance()
	}
}

func (l *Lexer) drop_line() {
	for {
		if l.is_empty() {
			return
		}

		c := rune(l.source[l.cur])
		if c == '\n' {
			break
		}

		l.advance()
	}

	if l.is_not_empty() {
		l.advance()
	}
}

func (l *Lexer) loc() Loc {
	return Loc{
		path: l.path,
		row:  l.row,
		col:  l.cur - l.bol,
	}
}

func (l *Lexer) remove_comments() error {
	for {
		l.trim_left()

		if l.is_empty() {
			return io.EOF
		}

		c := rune(l.source[l.cur])
		if c != '#' {
			return nil
		}

		l.drop_line()
	}
}

func (l *Lexer) next_token() (*Token, error) {
	err := l.remove_comments()
	if err != nil {
		return nil, err
	}

	if l.is_empty() {
		return nil, io.EOF
	}

	loc := l.loc()
	first := rune(l.source[l.cur])

	// Test Ident
	if unicode.IsLetter(first) {
		idx := l.cur
		for {
			if l.is_empty() {
				break
			}

			c := rune(l.source[l.cur])

			if unicode.IsNumber(c) || unicode.IsLetter(c) || c == '_' {
				l.advance()
				continue
			}

			break
		}

		val := l.source[idx:l.cur]
		return &Token{
			typ: TOKEN_IDENT,
			loc: loc,
			val: val,
		}, nil
	}

	// Test Literal Tokens
	lit, ok := LITERAL_TOKENS[first]
	if ok {
		l.advance()
		return &Token{
			typ: lit,
			loc: loc,
			val: string(first),
		}, nil
	}

	// Test String Literal
	if first == '"' {
		l.advance()
		start := l.cur
		end := l.cur

		for {
			if l.is_empty() {
				return nil, io.EOF
			}

			c := rune(l.source[l.cur])
			l.advance()

			if c != '"' {
				end += 1
				continue
			}

			return &Token{
				typ: TOKEN_STRING,
				loc: loc,
				val: l.source[start:end],
			}, nil
		}
	}

	// Test Number Literal
	if unicode.IsNumber(first) {
		start := l.cur

		for {
			l.advance()

			if l.is_empty() {
				return nil, io.EOF
			}

			c := rune(l.source[l.cur])
			if unicode.IsNumber(c) {
				continue
			}

			return &Token{
				typ: TOKEN_NUMBER,
				loc: loc,
				val: l.source[start:l.cur],
			}, nil
		}
	}

	panic("unknown lexable")
}
