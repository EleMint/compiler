package internal

import "fmt"

type TokenType uint

const (
	TOKEN_IDENT TokenType = iota
	TOKEN_OPAREN
	TOKEN_CPAREN
	TOKEN_OCURLY
	TOKEN_CCURLY
	TOKEN_SEMICOLON
	TOKEN_NUMBER
	TOKEN_STRING
	TOKEN_RETURN
)

func (tt TokenType) String() string {
	switch tt {
	case TOKEN_IDENT:
		return "IDENT"
	case TOKEN_OPAREN:
		return "OPAREN"
	case TOKEN_CPAREN:
		return "CPAREN"
	case TOKEN_OCURLY:
		return "OCURLY"
	case TOKEN_CCURLY:
		return "CCURLY"
	case TOKEN_SEMICOLON:
		return "SEMICOLON"
	case TOKEN_NUMBER:
		return "NUMBER"
	case TOKEN_STRING:
		return "STRING"
	case TOKEN_RETURN:
		return "RETURN"
	default:
		return "UNKNOWN"
	}
}

var LITERAL_TOKENS = map[rune]TokenType{
	'(': TOKEN_OPAREN,
	')': TOKEN_CPAREN,
	'{': TOKEN_OCURLY,
	'}': TOKEN_CCURLY,
	';': TOKEN_SEMICOLON,
}

type Token struct {
	typ TokenType
	loc Loc
	val string
}

func (t Token) String() string {
	return fmt.Sprintf("{Type: %s, Value: %s, Loc: %s}", t.typ, t.val, t.loc)
}
