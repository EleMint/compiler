package internal

import (
	_ "embed"
	"fmt"
	"io"
)

// Data represents a read only value
//
// example: `ident db "Hello, World", END_OF_STRING`
type Data struct {
	Symbol string // "ident"
	Type   string // "db", "dw", "dq"
	Value  string // "Hello, World!", END_OF_STRING
}

// Buffer represents a wr value
//
// example: `ident: resb 4`
type Buffer struct {
	Symbol string // "ident"
	Type   string // "resb", "resw", "resq"
	Quant  uint   // 4
}

func WriteData(w io.Writer, data []Data) {
	fmt.Fprintln(w, "section .data")

	for _, d := range data {
		fmt.Fprintf(w, "\t%s %s %s\n", d.Symbol, d.Type, d.Value)
	}

	fmt.Fprintln(w, "")
}

func WriteBuffer(w io.Writer, buffer []Buffer) {
	fmt.Fprintln(w, "section .bss")

	for _, buf := range buffer {
		fmt.Fprintf(w, "\t%s: %s %d\n", buf.Symbol, buf.Type, buf.Quant)
	}

	fmt.Fprintln(w, "")
}

//go:embed boiler/text.asm
var text_boiler string

func WriteLines(w io.Writer, lines []string) {
	fmt.Fprintln(w, text_boiler)

	for _, l := range lines {
		fmt.Fprintf(w, "%s\n", l)
	}

	fmt.Fprintln(w, "")
}
