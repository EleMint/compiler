package internal

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
)

type Type uint

const (
	TYPE_UNKNOWN Type = iota
	TYPE_VOID
	TYPE_INT
)

func (t Type) String() string {
	switch t {
	case TYPE_VOID:
		return "void"
	case TYPE_INT:
		return "int"
	default:
		return "unknown"
	}
}

type Parser struct {
	l *Lexer
}

func NewParser(input string) *Parser {
	l, err := NewLexer(input)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to create lexer: %s\n", err)
		os.Exit(1)
	}

	return &Parser{
		l: l,
	}
}

func (p *Parser) expect(types ...TokenType) (*Token, error) {
	tok, err := p.l.next_token()
	if err != nil {
		return nil, err
	}

	if tok == nil {
		return nil, errors.New("empty token")
	}

	for _, typ := range types {
		if tok.typ == typ {
			return tok, nil
		}
	}

	return nil, fmt.Errorf("expected token types %v; got %s at %s", types, tok.typ, tok.loc)
}

func (p *Parser) Type() (Type, error) {
	tok, err := p.expect(TOKEN_IDENT)
	if err != nil {
		return TYPE_UNKNOWN, err
	}

	switch tok.val {
	case "void":
		return TYPE_VOID, nil
	case "int":
		return TYPE_INT, nil
	default:
		return TYPE_UNKNOWN, errors.New("unknown type")
	}
}

func (p *Parser) Args() ([]Token, error) {
	_, err := p.expect(TOKEN_OPAREN)
	if err != nil {
		return nil, err
	}

	args := []Token{}

	for {
		expr, err := p.expect(TOKEN_STRING, TOKEN_NUMBER, TOKEN_CPAREN)
		if err != nil {
			return nil, err
		}

		if expr.typ == TOKEN_CPAREN {
			break
		}

		args = append(args, *expr)
	}

	return args, nil
}

func (p *Parser) Block() ([]Stmt, error) {
	_, err := p.expect(TOKEN_OCURLY)
	if err != nil {
		return nil, err
	}

	block := []Stmt{}

	for {
		name, err := p.expect(TOKEN_IDENT, TOKEN_CCURLY)
		if err != nil {
			return nil, err
		}

		if name.typ == TOKEN_CCURLY {
			break
		}

		if name.val == "return" {
			expr, err := p.expect(TOKEN_NUMBER, TOKEN_STRING)
			if err != nil {
				return nil, err
			}

			ret := Return{
				expr: *expr,
			}

			block = append(block, ret)
		} else {
			args, err := p.Args()
			if err != nil {
				return nil, err
			}

			call := NewFuncCall(*name, args)
			block = append(block, call)
		}
	}

	return block, nil
}

func (p *Parser) Func() (*Func, error) {
	ret, err := p.Type()
	if err != nil {
		return nil, err
	}

	if ret != TYPE_INT {
		panic(ret)
	}

	name, err := p.expect(TOKEN_IDENT)
	if err != nil {
		return nil, err
	}

	_, err = p.expect(TOKEN_OPAREN)
	if err != nil {
		return nil, err
	}

	_, err = p.expect(TOKEN_CPAREN)
	if err != nil {
		return nil, err
	}

	body, err := p.Block()
	if err != nil {
		return nil, err
	}

	return &Func{
		name: *name,
		body: body,
	}, nil
}

func (p *Parser) Program() ([]string, []Data, []Buffer, error) {
	f, err := p.Func()
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not parse func")
	}

	return f.Lines(), f.Data(), f.Buffer(), nil
}
