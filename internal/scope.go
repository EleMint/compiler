package internal

type Symbol struct {
	Name  string
	Type  Type
	Value interface{}
}

func NewSymbol(name string, typ Type, val interface{}) Symbol {
	return Symbol{
		Name:  name,
		Type:  typ,
		Value: val,
	}
}

type Scope struct {
	parent  *Scope
	symbols map[string]Symbol
}

func NewScope(parent *Scope) *Scope {
	return &Scope{
		parent:  parent,
		symbols: map[string]Symbol{},
	}
}

func (s *Scope) SetSymbol(sym Symbol) {
	s.symbols[sym.Name] = sym
}

func (s *Scope) GetSymbol(name string) (found *Symbol, ok bool) {
	f, ok := s.symbols[name]
	if ok {
		found = &f
		return
	}

	if s.parent != nil {
		found, ok = s.parent.GetSymbol(name)
	}

	return
}

func (s *Scope) DeleteSymbol(name string) {
	delete(s.symbols, name)
}
