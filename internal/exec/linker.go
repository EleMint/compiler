package exec

import (
	"os"
	"os/exec"

	"github.com/pkg/errors"
)

func LinkObj(outPath string, inPath string, clean bool) error {
	cmd := exec.Command("ld", "-s", "-o", outPath, inPath)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		return errors.Wrap(err, "ld")
	}

	if clean {
		err = os.Remove(inPath)
		if err != nil {
			return errors.Wrapf(err, "could not remove object file %s", inPath)
		}
	}

	return nil
}
