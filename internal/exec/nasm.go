package exec

import (
	"os"
	"os/exec"

	"github.com/pkg/errors"
)

func CompileAsm(outPath string, inPath string, clean bool) error {
	cmd := exec.Command("nasm", "-f", "elf64", "-o", outPath, inPath)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		return errors.Wrap(err, "nasm")
	}

	if clean {
		err = os.Remove(inPath)
		if err != nil {
			return errors.Wrapf(err, "failed to remove assembly file %s", inPath)
		}
	}

	return nil
}
