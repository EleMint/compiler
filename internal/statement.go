package internal

import (
	"fmt"
	"strings"
)

type StmtType uint

const (
	STMT_RETURN StmtType = iota
	STMT_FUNC_CALL
	STMT_FUNC
)

type Stmt interface {
	Kind() StmtType
	Type() Type
	String() string
	Lines() []string
	Data() []Data
	Buffer() []Buffer
}

type Return struct {
	typ  Type
	expr Token
}

func (ret Return) Kind() StmtType {
	return STMT_RETURN
}

func (ret Return) Type() Type {
	return ret.typ
}

func (ret Return) String() string {
	return fmt.Sprintf("ret::%s", ret.expr)
}

func (ret Return) Lines() []string {
	return []string{
		fmt.Sprintf("  mov rax, %s", ret.expr.val),
		"  ret",
	}
}

func (ret Return) Data() []Data {
	return nil
}

func (ret Return) Buffer() []Buffer {
	return nil
}

type FuncCall struct {
	name   Token
	args   []Token
	ret    Type
	lines  []string
	data   []Data
	buffer []Buffer
}

var registers = []string{
	"rdi",
	"rsi",
	"rdx",
	"r10",
	"r8",
	"r9",
}

func NewFuncCall(name Token, args []Token) FuncCall {
	var lines []string
	var data []Data
	var buffer []Buffer

	for i, arg := range args {
		if arg.typ == TOKEN_STRING {
			d := Data{
				Symbol: fmt.Sprintf("_str%d", i),
				Type:   "db",
				Value:  fmt.Sprintf("\"%s\", NEW_LINE, END_OF_STRING", arg.val),
			}

			data = append(data, d)

			line := fmt.Sprintf("  mov %s, %s", registers[i], d.Symbol)
			lines = append(lines, line)
		} else if arg.typ == TOKEN_NUMBER {
			line := fmt.Sprintf("  mov %s, %s", registers[i], arg.val)
			lines = append(lines, line)
		} else {
			panic("unknown type")
		}
	}

	c := fmt.Sprintf("  call %s", name.val)
	lines = append(lines, c)

	return FuncCall{
		name:   name,
		args:   args,
		ret:    TYPE_INT,
		lines:  lines,
		data:   data,
		buffer: buffer,
	}
}

func (call FuncCall) Kind() StmtType {
	return STMT_FUNC_CALL
}

func (call FuncCall) Type() Type {
	return call.ret
}

func (call FuncCall) String() string {
	return fmt.Sprintf("call::%s(%s)", call.name.val, call.args)
}

func (call FuncCall) Lines() []string {
	return call.lines
}

func (call FuncCall) Data() []Data {
	return call.data
}

func (call FuncCall) Buffer() []Buffer {
	return call.buffer
}

type Func struct {
	ret  Type
	name Token
	body []Stmt
}

func (f Func) Type() StmtType {
	return STMT_FUNC
}

func (f Func) String() string {
	sb := strings.Builder{}

	sb.WriteString(fmt.Sprintf("func::%s::%s\n", f.name.val, f.ret))

	for _, stmt := range f.body {
		sb.WriteString(fmt.Sprintf("\t%s\n", stmt))
	}

	return sb.String()
}

func (f Func) Lines() []string {
	var lines []string

	name := fmt.Sprintf("%s:", f.name.val)
	lines = append(lines, name)

	for _, stmt := range f.body {
		lines = append(lines, stmt.Lines()...)
	}

	return lines
}

func (f Func) Data() []Data {
	var data []Data

	for _, stmt := range f.body {
		data = append(data, stmt.Data()...)
	}

	return data
}

func (f Func) Buffer() []Buffer {
	var buffer []Buffer

	for _, stmt := range f.body {
		buffer = append(buffer, stmt.Buffer()...)
	}

	return buffer
}
