section .text
	global _start

SYS_WRITE     equ 1
SYS_EXIT      equ 60

STDOUT        equ 1

END_OF_STRING equ 0
NEW_LINE      equ 10

_start:
	call main

	mov rdi, rax
	mov rax, SYS_EXIT
	syscall
	ret

write_char_ptr:
	push rax
	push rdx

	mov rax, SYS_WRITE
	mov rdx, 1
	syscall

	pop rdx
	pop rax
	ret

write_str:
  push rbx ; save rbx
  push rsi ; save rsi
  push rdi ; save rdi

  mov rsi, rdi
  mov rdi, STDOUT

.loop:
  ; get next char from input string
  mov bl, [rsi]
  ; return if char is END_OF_STRING
  cmp bl, END_OF_STRING
  je .exit

  ; write char
  call write_char_ptr

  inc rsi
  jmp .loop

.exit:
  pop rdi ; restore rdi
  pop rsi ; restore rsi
  pop rbx ; restore rbx
  ret
