package internal

import "fmt"

type Loc struct {
	path string
	row  uint
	col  uint
}

func (l Loc) String() string {
	return fmt.Sprintf("%s:%d:%d", l.path, l.row+1, l.col+1)
}
