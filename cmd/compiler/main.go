package main

import (
	"compiler/internal"
	"compiler/internal/exec"
	_ "embed"
	"flag"
	"fmt"
	"io"
	"os"
	exe "os/exec"
	"path"
)

func init() {
	flag.Usage = usage(os.Stdout)
}

func main() {
	var output string
	flag.StringVar(&output, "o", "./main", "output executable")

	var debug bool
	flag.BoolVar(&debug, "d", true, "debug mode")

	var clean bool
	flag.BoolVar(&clean, "c", true, "remove .asm and .o files")

	var run bool
	flag.BoolVar(&run, "r", false, "run exe after compiling")

	flag.Parse()

	tail := flag.Args()
	if len(tail) < 1 {
		fmt.Fprintf(os.Stderr, "no input file provided\n\n")
		usage(os.Stderr)()
		fmt.Fprintln(os.Stderr, "")
		os.Exit(1)
	}

	input := tail[0]

	p := internal.NewParser(input)
	lines, data, buffer, err := p.Program()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to parse program: %s\n", err)
		os.Exit(1)
	}

	output_asm := fmt.Sprintf("%s.asm", output)
	out, err := os.OpenFile(
		output_asm,
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY,
		0660,
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open output file (%s) for writing: %s\n", output_asm, err)
		os.Exit(1)
	}

	internal.WriteLines(out, lines)
	internal.WriteData(out, data)
	internal.WriteBuffer(out, buffer)

	err = out.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to close output file %s: %s\n", output_asm, err)
		os.Exit(1)
	}

	output_obj := fmt.Sprintf("%s.o", output)

	err = exec.CompileAsm(output_obj, output_asm, clean)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to compile output assembly file %s: %s\n", output_asm, err)
		os.Exit(1)
	}

	err = exec.LinkObj(output, output_obj, clean)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to link object file %s to executable %s: %s\n", output_obj, output, err)
		os.Exit(1)
	}

	if run {
		dir, err := os.Getwd()
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to get working dir of exe: %s\n", err)
		}

		cmd := exe.Command(path.Join(dir, output))
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		err = cmd.Run()
		if err != nil {
			e, ok := err.(*exe.ExitError)
			if ok {
				os.Exit(e.ExitCode())
			}
		}

		os.Exit(0)
	}
}

func usage(w io.Writer) func() {
	return func() {
		fmt.Fprintf(w, "Usage of %s:\n", os.Args[0])

		fmt.Fprintln(w, "\n\t./exe [options] <input file>")

		fmt.Fprintln(w, "\n\tOptions:")

		fmt.Fprintln(w, "\t\t-h\t\tprint usage information")

		flag.VisitAll(func(f *flag.Flag) {
			fmt.Fprintf(w, "\t\t-%s\t\t%s\n", f.Name, f.Usage)
		})
	}
}
