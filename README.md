# Compiler

## CLI

- Usage:
  ```bash
  emt [options] <path>
  ```

  - options
    - `o`: output executable
    - `d`: enable debug
    - `c`: "clean" removes [.asm] and [.o] files
    - `r`: run executable after compiling

## Required

### `nasm` [Netwide Assembler] v2.15.0

### `ld` The GNU Linker v2.38 ([binutils])

[Netwide Assembler]: https://nasm.us
[binutils]: https://gnu.org/software/binutils

